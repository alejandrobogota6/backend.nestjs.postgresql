import { IsString } from "class-validator";
import { TaskCategory } from "../enums";

export class CreateTaskDto {

    @IsString()
    title: string;

    @IsString()
    content: string;

}