import { CreateTaskDto } from "./create-task.dto";
import { PartialType, OmitType } from '@nestjs/mapped-types';

//Campos opciones con el PartialType de extension de anterior DTO
export class EditTaskDto extends PartialType(
    OmitType(CreateTaskDto, ['title'] as const)
    ) {
    
}