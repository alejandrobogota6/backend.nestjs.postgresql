export enum TaskCategory {
    'TECHNOLOGY',
    'LIFESTYLE',
    'CODING',
}