import { Body, Controller, Delete, Get, Param, ParseIntPipe, Post, Put } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { CreateTaskDto } from './dtos/create-task.dto';
import { EditTaskDto } from './dtos/edit-task.dto';
import { TaskService } from './task.service';

@ApiTags('Task Module')
@Controller('task')
export class TaskController {

    constructor(private readonly taskService: TaskService) {}

    @Get()
    async getMany() {
        const data = await this.taskService.getMany();
        return {
            message: 'Peticion GetAll Correcta',
            data
        }
    }

    @Get(':id')
    getOne(@Param('id', ParseIntPipe) id: number) {
        return this.taskService.getOne(id);
    }

    @Get('/One/:title/:cont')
    getOneTask(@Param('title') title: string , @Param('cont') cont: string){
        return this.taskService.getOneTask(title, cont);
    }

    @Post()
    async createOne(
        @Body() dto: CreateTaskDto
    ) {
        const data = await this.taskService.createOne(dto);
        return {
            message: 'Peticion Crear Correcta',
            data
        }
    }

    @Put(':id')
    editOne(@Param('id') id: number,
        @Body() dto: EditTaskDto) {
        return this.taskService.editOne(id, dto);
    }

    @Delete(':id')
    deleteOne(@Param('id') id: number) {
        return this.taskService.deleteOne(id);
    }
}
