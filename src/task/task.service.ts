import { Injectable, NotFoundException, Req } from '@nestjs/common';
import { CreateTaskDto } from './dtos/create-task.dto';
import { EditTaskDto } from './dtos/edit-task.dto';
import { Repository } from 'typeorm';
import { Task } from './entities/task.entity';
import { InjectRepository } from '@nestjs/typeorm'
import { title } from 'process';

@Injectable()
export class TaskService {

    constructor(
        @InjectRepository(Task)
        private readonly taskRepository : Repository<Task>
    ){

    }

    async getMany(): Promise<Task[]> {
        return await this.taskRepository.find()
    }

    async getOne(id: number) {
        const data = await this.taskRepository.findOne({ where: { id: id }})
        if (!data) throw new NotFoundException()
        return data
    }

    async getOneTask (title: string, cont: string){
        const data = await this.taskRepository.findOne({ where: { title: title , content: cont}})
        if (!data) throw new NotFoundException()
        return data
    }

    async createOne(dto: CreateTaskDto) {
        const task = this.taskRepository.create(dto);
        return await this.taskRepository.save(task);
    }

    async editOne(id: number, dto: EditTaskDto) {
        const task = await this.taskRepository.findOne({ where: { id: id }});

        if (!task) throw new NotFoundException('Task does not exist');
        const editedTask = Object.assign(task, dto);
        return await this.taskRepository.save(editedTask);
    }

    async deleteOne(id: number){
        return await this.taskRepository.delete(id);
    }
}
